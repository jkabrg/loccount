define method main () => ()
  // Your program starts here...

let i = <integer>;
let j = <integer>;
let candidates = make(<vector>, size: 1000);
for (i from 0 below 1000)
  candidates[i] := 1;
end for;
candidates[0] := 0;
candidates[1] := 0;
i := 0;
while (i < 1000)
  while (i < 1000 & candidates[i] = 0)
    i := i + 1;
  end while;
  if (i < 1000)
    j := 2;
    while ((i * j) < 1000)
      candidates[i * j] := 0;
      j := j + 1;
    end while;
    i := i + 1;
  end if;
end while;

for (i from 0 below 1000)
  if (candidates[i] ~= 0)
    format-out("%d is prime\n",i);
  end if;
end for;

end method main;

begin
  main();
end;
