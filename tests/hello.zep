// From https://docs.zephir-lang.com/0.11/en/tutorial
// SLOC: 8, LLOC: 2, all: 13, comments: 2
namespace Utils;

class Greeting
{

    public static function say()
    {
        echo "hello world!";
    }

}
