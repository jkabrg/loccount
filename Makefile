# Makefile for loccount
# You must have the Go compiler and tools installed to build this software.

VERS=$(shell sed <loccount -n -e '/version string *= *\(.*\)/s//\1/p')

loccount: loccount.go
	go build

clean:
	go clean
	rm -f *.html *.1 *~ tests/*~

install: loccount
	go install

check: loccount
	@loccount -s >/dev/null
	@(./loccount -i tests; ./loccount -u tests) | diff -u check.good -
	@echo "No check output (other than this line) is good news"

testbuild: loccount
	@rm -f tests/*~; (./loccount -i tests; ./loccount -u tests) >check.good

gofmt:
	gofmt -s -d loccount.go | patch -p0

# In case the logo ever needs to be scaled, the SVG original can be found here:
# https://commons.wikimedia.org/wiki/File:Tally_marks-Five-bar_Gate.svg
SOURCES = README COPYING NEWS control loccount.go loccount.adoc hacking.adoc \
		design-notes.adoc Makefile TODO loccount-logo.png check.good tests/

.SUFFIXES: .html .adoc .1

# Requires asciidoc and xsltproc/docbook stylesheets.
.adoc.1:
	a2x --doctype manpage --format manpage $<
.adoc.html:
	asciidoc $<

VERS=$(shell sed <loccount.go -n -e '/.*version.*= *\(.*\)/s//\1/p')

version:
	@echo $(VERS)

# You can profile this code with, e.g.
#
# loccount -cpuprofile /tmp/loc.prof .
# go tool pprof loccount /tmp/loc.prof
#
# "top20" is a useful command to start with in viewing results.
#
# Also, remember to check the report card occasionally:
# https://goreportcard.com/report/gitlab.com/esr/loccount#L1

# Report which languages lack lloc support
sloc: loccount
	loccount -i -s  | sort >/tmp/sloc$$; loccount -i -l | sort >/tmp/lloc$$
	comm -23 /tmp/sloc$$ /tmp/lloc$$
	rm -f /tmp/sloc$$ /tmp/lloc$$

loccount-$(VERS).tar.gz: $(SOURCES) loccount.1
	tar --transform='s:^:loccount-$(VERS)/:' --show-transformed-names -cvzf loccount-$(VERS).tar.gz $(SOURCES) loccount.1

dist: loccount-$(VERS).tar.gz

release: loccount-$(VERS).tar.gz loccount.html hacking.html
	shipper version=$(VERS) | sh -e -x

refresh: loccount.html
	shipper -N -w version=$(VERS) | sh -e -x
